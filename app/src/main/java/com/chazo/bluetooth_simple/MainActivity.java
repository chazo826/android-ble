package com.chazo.bluetooth_simple;

import android.bluetooth.BluetoothGattCharacteristic;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.polidea.rxandroidble.RxBleClient;
import com.polidea.rxandroidble.RxBleConnection;
import com.polidea.rxandroidble.RxBleDeviceServices;
import com.polidea.rxandroidble.internal.RxBleLog;
import com.polidea.rxandroidble.utils.ConnectionSharingAdapter;
import com.trello.rxlifecycle.components.support.RxAppCompatActivity;

import java.util.ArrayList;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static com.trello.rxlifecycle.android.ActivityEvent.PAUSE;

public class MainActivity extends RxAppCompatActivity implements View.OnClickListener{
    private static final String TAG = MainActivity.class.getName();
    private final int PERMISSION_REQUEST_CODE = 1;
    private final String READ_PREFIX = "obd : ";
    private BluetoothHelper bluetoothHelper;

    private Button connectBtn;
    private Button writeBtn;
    private Button readBtn;
    private TextView connectionStateTv;
    private EditText writeEdt;

    private ListView readListview;
    private ArrayAdapter<String> readAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        checkedLocationPermission();
        setBleClient();

        bluetoothHelper = BluetoothHelper.getInstance()
                .setRxBLE(setBleClient(), getIntent().getStringExtra("mac_addr"));

        bluetoothHelper.setPrepareConnectionObservable(prepareConnectionObservable());

        bluetoothHelper.getDiscoverService()
                .compose(bindUntilEvent(PAUSE))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        this::discoverComplete,
                        this::onConnectionFailure);

        connectBtn = findViewById(R.id.act_main_connect_btn);
        writeBtn = findViewById(R.id.act_main_write_btn);
        readBtn = findViewById(R.id.act_main_read_btn);
        connectionStateTv = findViewById(R.id.act_main_connection_state_tv);
        writeEdt = findViewById(R.id.act_main_write_edt);
        readListview = findViewById(R.id.act_main_read_listview);
        readAdapter =  new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, new ArrayList<String>());
        readListview.setAdapter(readAdapter);

        connectBtn.setOnClickListener(this);
        writeBtn.setOnClickListener(this);
        readBtn.setOnClickListener(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(bluetoothHelper.isConnected()){
            bluetoothHelper.triggerDisconnect();
        }
    }

    private void discoverComplete(RxBleDeviceServices services) {
        bluetoothHelper.setCharacteristic(services);
        //todo: 버튼 활성화
        connectBtn.setEnabled(true);
        connectionStateTv.setText("ready");
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if(id == R.id.act_main_connect_btn){
            if(bluetoothHelper.isConnected()){
                bluetoothHelper.triggerDisconnect();
            } else {
                readAdapter.clear();
                readAdapter.notifyDataSetChanged();

                bluetoothHelper.getConnectService()
                        .doOnSubscribe(() -> connectionStateTv.setText("Connecting..."))
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                this::updateUI,
                                this::onConnectionFailure,
                                this::onConnectionFinished
                        );
            }
        } else if(id == R.id.act_main_read_btn){
            if(bluetoothHelper.isConnected()){
                bluetoothHelper.getReadService()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                bytes -> readAdapter.add(READ_PREFIX + HexString.bytesToHex(bytes)),
                                this::onReadFailure
                        );
            }
        } else if(id == R.id.act_main_write_btn){
            if(bluetoothHelper.isConnected()){
                bluetoothHelper.getWriteService(getInputBytes())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                bytes -> onWriteSuccess(),
                                this::onWriteFailure
                        );
            }
        }
    }

    private void updateUI(BluetoothGattCharacteristic characteristic){
        boolean isCharacteristic = characteristic != null;
        connectBtn.setText(isCharacteristic? "disconnect" : "connect");
        connectionStateTv.setText(isCharacteristic? "connected" : "connection failed");
        readBtn.setEnabled(isCharacteristic);
        writeBtn.setEnabled(isCharacteristic);
    }

    private Observable<RxBleConnection> prepareConnectionObservable() {
        return bluetoothHelper.getBleDevice()
                .establishConnection(true)
                .takeUntil(bluetoothHelper.getDisconnectTriggerSubject())
                .compose(bindUntilEvent(PAUSE))
                .compose(new ConnectionSharingAdapter());
    }

    private void onConnectionFailure(Throwable throwable) {
        //noinspection ConstantConditions
        Toast.makeText(this, "Connection error: " + throwable, Toast.LENGTH_LONG).show();
        updateUI(null);
        Log.e(TAG, "Connection error: " + throwable);
    }

    private void onConnectionFinished() {
        updateUI(null);
    }

    private void onReadFailure(Throwable throwable) {
        //noinspection ConstantConditions
        Toast.makeText(this, "Read error: " + throwable, Toast.LENGTH_LONG).show();
        Log.e(TAG, "Read error: " + throwable);
    }

    private void onWriteSuccess() {
        //noinspection ConstantConditions
        Toast.makeText(this, "Write success", Toast.LENGTH_SHORT).show();

    }

    private void onWriteFailure(Throwable throwable) {
        //noinspection ConstantConditions
        Toast.makeText(this, "Write error: " + throwable, Toast.LENGTH_LONG).show();
        Log.e(TAG, "Write error: " + throwable);
    }

    private void checkedLocationPermission() {
        int permission = ContextCompat.checkSelfPermission(this, ACCESS_COARSE_LOCATION);
        if (permission == PackageManager.PERMISSION_DENIED) {
            //권한 없음
            ActivityCompat.requestPermissions(this,
                    new String[]{ACCESS_COARSE_LOCATION},
                    PERMISSION_REQUEST_CODE);
        }
    }

    private byte[] getInputBytes() {
        try {
            return HexString.hexToBytes(writeEdt.getText().toString());
        }catch (IllegalArgumentException e){
            Toast.makeText(this, "Hex사이즈(짝수사이즈)에 맞게 해주세요.\n00바이트 보냄", Toast.LENGTH_SHORT).show();
            return new byte[]{0, 0};
        }
    }

    private RxBleClient setBleClient() {
        RxBleClient bleClient = RxBleClient.create(this);
        RxBleClient.setLogLevel(RxBleLog.DEBUG);
        return bleClient;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_DENIED) {
                finish();
            }
        }
    }


}
