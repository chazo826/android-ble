package com.chazo.bluetooth_simple;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Choi Jaeung on 2018-01-31.
 */

public class DeviceSelectActivity extends AppCompatActivity {
    private BluetoothHelper bluetoothHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_select);

        bluetoothHelper = BluetoothHelper.getInstance();

        ListView deviceLv = findViewById(R.id.act_devsel_listview);

        Button initBtn = findViewById(R.id.act_init_btn);
        initBtn.setOnClickListener(v -> {
            if(bluetoothHelper.checkedBluetoothEnable() == false){
                startActivity(new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE));
            } else {
                initBtn.setVisibility(View.GONE);
                deviceLv.setVisibility(View.VISIBLE);

                setListView(deviceLv);
            }
        });


    }
    private void setListView(ListView listView){
        List<String> values = new ArrayList<>();
        List<BluetoothDevice> devices = bluetoothHelper.getPairedDevices();
        for(BluetoothDevice device : devices){
            values.add(device.getName());
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, values);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener((adapterView, view, position, id) -> {
            Intent intent = new Intent(DeviceSelectActivity.this, MainActivity.class);
            intent.putExtra("mac_addr", devices.get(position).getAddress());
            startActivity(intent);
            finish();
        });
    }
}
