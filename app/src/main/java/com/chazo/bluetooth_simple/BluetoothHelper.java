package com.chazo.bluetooth_simple;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;

import com.polidea.rxandroidble.RxBleClient;
import com.polidea.rxandroidble.RxBleConnection;
import com.polidea.rxandroidble.RxBleDevice;
import com.polidea.rxandroidble.RxBleDeviceServices;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.subjects.PublishSubject;

/**
 * Created by Choi Jaeung on 2018-01-31.
 */

public class BluetoothHelper {
    private static BluetoothHelper instance = new BluetoothHelper();
    private BluetoothAdapter bluetoothAdapter;
    private RxBleClient bleClient;
    private RxBleDevice bleDevice;
    private BluetoothGattCharacteristic characteristic;
    private PublishSubject<Void> disconnectTriggerSubject = PublishSubject.create();
    private Observable<RxBleConnection> connectionObservable;

    private BluetoothHelper(){
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    }

    public BluetoothHelper setRxBLE(RxBleClient bleClient, String macAddr){
        this.bleClient = bleClient;
        bleDevice = bleClient.getBleDevice(macAddr);
        return this;
    }

    public void setPrepareConnectionObservable(Observable<RxBleConnection> observable){
        connectionObservable = observable;
    }

    public static BluetoothHelper getInstance(){
        return instance;
    }

    public Observable<RxBleDeviceServices> getDiscoverService(){
        return bleDevice.establishConnection(false)
                .flatMap(RxBleConnection::discoverServices)
                .first(); // Disconnect automatically after discovery
    }

    public Observable<BluetoothGattCharacteristic> getConnectService(){
        return connectionObservable
                .flatMap(RxBleConnection::discoverServices)
                .flatMap(rxBleDeviceServices -> rxBleDeviceServices.getCharacteristic(characteristic.getUuid()));
    }

    public Observable<byte[]> getReadService() {
        return connectionObservable
                .flatMap(rxBleConnection -> rxBleConnection.readCharacteristic(characteristic.getUuid()));
    }

    public Observable<byte[]> getWriteService(byte[] inputByte){
        return connectionObservable
                .flatMap(rxBleConnection -> rxBleConnection.writeCharacteristic(characteristic.getUuid(), inputByte));
    }


    public void setCharacteristic(RxBleDeviceServices services){
        for (BluetoothGattService service : services.getBluetoothGattServices()) {
            final List<BluetoothGattCharacteristic> characteristics = service.getCharacteristics();

            for (BluetoothGattCharacteristic characteristic : characteristics) {
                if(isCharacteristicReadable(characteristic) && isCharacteristicWritable(characteristic)){
                    this.characteristic = characteristic;
                    return;
                }
            }
        }
    }

    public void triggerDisconnect() {
        disconnectTriggerSubject.onNext(null);
    }

    public boolean isConnected() {
        return bleDevice.getConnectionState() == RxBleConnection.RxBleConnectionState.CONNECTED;
    }

    public boolean checkedBluetoothEnable(){
        return !(bluetoothAdapter == null || bluetoothAdapter.enable() == false);
    }

    public List<BluetoothDevice> getPairedDevices(){
        return new ArrayList<>(bluetoothAdapter.getBondedDevices());
    }

    public PublishSubject<Void> getDisconnectTriggerSubject(){
        return disconnectTriggerSubject;
    }

    public RxBleDevice getBleDevice(){
        return bleDevice;
    }

    private boolean isCharacteristicReadable(BluetoothGattCharacteristic characteristic) {
        return ((characteristic.getProperties() & BluetoothGattCharacteristic.PROPERTY_READ) != 0);
    }

    private boolean isCharacteristicWritable(BluetoothGattCharacteristic characteristic) {
        return (characteristic.getProperties() & (BluetoothGattCharacteristic.PROPERTY_WRITE
                | BluetoothGattCharacteristic.PROPERTY_WRITE_NO_RESPONSE)) != 0;
    }
}
